﻿using STFMJournal.Business.Entities.MajorEntities;
using STFMJournal.Data.Mongo.Repositories;
using STFMJournal.Data.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace STFMJournal.WebAPI.Controllers
{
    public class ArticlesController : ApiController
    {
        private IArticleRepository articleRepo;

        public ArticlesController(IArticleRepository _articleRepo)
        {
            articleRepo = _articleRepo;
        }

        public Article Get(string id)
        {
            try
            {
                return articleRepo.GetArticleById(id);
            }
            catch (Exception ex)
            {
                //Log Error
                //Need to figure out what to return in case of exception
                return null;
            }
        }
    }
}
