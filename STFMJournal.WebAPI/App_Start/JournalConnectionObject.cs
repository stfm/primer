﻿using STFMJournal.Data.Mongo;
using STFMJournal.WebAPI.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STFMJournal.WebAPI.App_Start
{
    public class JournalConnectionObject : IMongoConnectionObject
    {
        public string ConnectionString
        {
            get
            {
                return Settings.Default.JournalConnectionString;
            }

            set
            {
                ConnectionString = value;
            }
        }

        public string DatabaseName
        {
            get
            {
                return Settings.Default.JournalDatabaseName;
            }

            set
            {
                DatabaseName = value;
            }
        }
    }
}