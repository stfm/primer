﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using STFMJournal.Common.Mock;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Common.Test
{
    [TestClass]
    public class JournalNoIssuesMockTest
    {
        JournalNoIssuesMock mocker;
        public JournalNoIssuesMockTest()
        {
            mocker = new JournalNoIssuesMock();
        }

        [TestMethod]
        public void GetJournalNoIssues()
        {
            var newNoIssuesJournal = mocker.CreateNoIssueJournal_NoImages();
            Console.WriteLine(JsonConvert.SerializeObject(newNoIssuesJournal)); 
        }
    }
}
