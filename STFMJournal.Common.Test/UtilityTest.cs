﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STFMJournal.Common.Utility;

namespace STFMJournal.Common.Test
{
    [TestClass]
    public class UtilityTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void GuardAgainstNullOrEmpty_NullString_ThrowsException()
        {
            //Arrange
            string value = null;

            //Act
            Guard.GuardAgainstNullOrEmpty(null, "Can't be null!");
        }
    }
}
