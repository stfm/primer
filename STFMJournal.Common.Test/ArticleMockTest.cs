﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using Newtonsoft.Json;
using STFMJournal.Business.Entities.MajorEntities;
using STFMJournal.Common.Mock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Common.Test
{
    [TestClass]
    public class ArticleMockTest
    {
        ArticleMock mocker;
        
        public ArticleMockTest()
        {
            mocker = new ArticleMock();
            JsonWriterSettings.Defaults.Indent = true;
        }

        [TestMethod]
        public void CreatePrimerArticle()
        {
            Article primerArticle = mocker.GeneratePRiMERArticle();
            Console.WriteLine(primerArticle.ToJson());
        }


    }
}
