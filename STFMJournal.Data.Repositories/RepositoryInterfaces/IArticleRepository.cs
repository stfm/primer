﻿using MongoDB.Bson;
using STFMJournal.Business.Entities.MajorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Data.Repositories.RepositoryInterfaces
{
    public interface IArticleRepository
    {
        Article GetArticleById(ObjectId id);
        Article GetArticleById(string id);
        void AddArticle(Article article);
        void DeleteArticle(ObjectId id);
        void UpdateArticle(ObjectId id, Article article);
    }
}
