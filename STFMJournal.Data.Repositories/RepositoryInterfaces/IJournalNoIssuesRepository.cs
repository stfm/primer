﻿using MongoDB.Bson;
using STFMJournal.Business.Entities.MajorEntities;
using STFMJournal.Business.Entities.MinorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Data.Repositories.RepositoryInterfaces
{
    public interface IJournalNoIssuesRepository
    {
        void Create(JournalNoIssues journal);
        void Update(JournalNoIssues journal);
        void Delete(ObjectId id);
        JournalNoIssues GetById(ObjectId id);
        List<JournalNoIssues> GetAll();


        List<Issue> GetRecentIssues(ObjectId journalId, int numberOfIssuesToGet);
        List<ArticleSummary> GetRecentArticles(ObjectId journalId, int numberOfArticlesToGet);
        List<ArticleSummary> GetArticlesByIssue(ObjectId journalId, int volume, int issue);

        
    }
}
