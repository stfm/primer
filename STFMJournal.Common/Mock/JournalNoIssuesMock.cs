﻿using STFMJournal.Business.Entities.MajorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Common.Mock
{
    public class JournalNoIssuesMock
    {
        ArticleMock mocker;
        public JournalNoIssuesMock()
        {
            mocker = new ArticleMock();
        }

        public JournalNoIssues CreateNoIssueJournal_NoImages()
        {
            JournalNoIssues newNoIssuesJournal = JournalNoIssues.Create(1, "PRiMER", "PRiME Your Family Medicine Knowledge" ,"STFM", "Family Medicine", "Great journal about family medicine", null);
            newNoIssuesJournal.AddVolume("2014");
            newNoIssuesJournal.AddVolume("2015");
            newNoIssuesJournal.AddVolume("2016");

            newNoIssuesJournal.AddArticleSummary("2016", "Family Medicine Best Practices", null, getAuthors(), DateTimeOffset.Now, "Letter To Editor", "507f1f77bcf86cd799439011");
            newNoIssuesJournal.AddArticleSummary("2016", "Lung Cancer", "What's Your Risk Factor?", getAuthors(), DateTimeOffset.Now.AddMonths(-2), "Original Article", "507f1f77bcf86cd799439012");

            newNoIssuesJournal.AddArticleSummary("2015", "Memory Loss", "What We Know and Don't Know", getAuthors(), DateTimeOffset.Now.AddYears(-1), "Op Ed", "507f1f77bcf86cd799439013");
            newNoIssuesJournal.AddArticleSummary("2015", "Patient Retention", "How to Ensure Patient Comfort", getAuthors(), DateTimeOffset.Now.AddYears(-1).AddMonths(-2), "Op Ed", "507f1f77bcf86cd799439014");
            newNoIssuesJournal.AddArticleSummary("2015", "Diagnosing the Flu", null,getAuthors(), DateTimeOffset.Now.AddYears(-1).AddMonths(-3), "Op Ed", "507f1f77bcf86cd799439017");
            newNoIssuesJournal.AddArticleSummary("2015", "Preventing Heart Disease", null, getAuthors(), DateTimeOffset.Now.AddYears(-1).AddMonths(-4), "Op Ed", "507f1f77bcf86cd799439018");

            newNoIssuesJournal.AddArticleSummary("2014", "Quotas", "Do We Have to Have Them?", getAuthors(), DateTimeOffset.Now.AddYears(-2), "Family Medicine", "507f1f77bcf86cd799439015");
            newNoIssuesJournal.AddArticleSummary("2014", "Affordable Care Act", "What it Means for Your Practice", getAuthors(), DateTimeOffset.Now.AddYears(-2).AddMonths(-2), "Letter To Editor", "507f1f77bcf86cd799439016");

            return newNoIssuesJournal;
        }

        private List<string> getAuthors()
        {
            return new List<string>()
            {
                "John D. Doe, Phd",
                "Douglas M. Schultz, Md",
                "Sarah Ann O'Connor, Md, Phd"
            };
        }
    }
}
