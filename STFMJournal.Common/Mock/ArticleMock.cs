﻿using STFMJournal.Business.Entities.MajorEntities;
using STFMJournal.Business.Entities.MinorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Common.Mock
{
    public class ArticleMock
    {
        public Article GeneratePRiMERArticle()
        {
            return Article.Create(1, "Bird Flu Epidemic", "Is there really a crisis?", "PRiMER", DateTimeOffset.Now,
                GenerateSections(), GenerateAuthors(), GenerateMetricProviders());
        }

        public static List<Section> GenerateSections()
        {
            return new List<Section> {
                new Section()
                {
                    Title = "Abstract",
                    SubSections = new List<SubSection>
                    {
                        new SubSection()
                        {
                            Title = "Methods",
                            ContentAreas = new List<ContentArea>
                            {
                                new ContentArea()
                                {
                                    ContentType = Business.Entities.Enums.ContentType.Text,
                                    Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dictum arcu tincidunt elementum ultricies. Etiam scelerisque augue sed maximus faucibus."
                                }
                            }

                        },
                        new SubSection()
                        {
                            Title = "Results",
                            ContentAreas = new List<ContentArea>
                            {
                                new ContentArea()
                                {
                                    ContentType = Business.Entities.Enums.ContentType.Text,
                                    Content = "Suspendisse eleifend sagittis metus a hendrerit. Mauris pellentesque mi lorem. Vestibulum eu odio mi. Sed ut vehicula neque, vel efficitur velit. Donec luctus, neque vel molestie dignissim, est nibh pellentesque erat, a consequat eros enim sed lectus. Nunc ac aliquam mi. Suspendisse pellentesque dictum leo, in molestie dui scelerisque quis. Nam tincidunt dui metus, non finibus magna iaculis fringilla. Quisque at risus augue. Suspendisse ac ligula pulvinar, cursus ligula maximus, fermentum dui. Donec vel massa ut ipsum pulvinar mollis. Aliquam erat volutpat."
                                }
                            }

                        }
                    }
                },
                new Section()
                {
                    Title = "",
                    SubSections = new List<SubSection>
                    {
                        new SubSection()
                        {
                            Title = "Conslusions",
                            ContentAreas = new List<ContentArea>
                            {
                                new ContentArea()
                                {
                                    ContentType = Business.Entities.Enums.ContentType.Text,
                                    Content = "Suspendisse ultricies nisi eros. Integer consectetur vitae sem in ornare. Fusce pharetra dapibus sem, quis pulvinar tellus tincidunt a. Vivamus sit amet eros blandit, mattis arcu et, facilisis lacus. Nulla sed luctus ipsum. Sed sed erat non arcu blandit placerat in id tellus. Etiam vehicula lacus quis lacus malesuada, quis varius mauris varius. Maecenas ornare pretium dignissim. Vivamus eget massa non purus bibendum convallis."
                                }
                            }

                        }
                    }
                }
            };

        }

        public static List<Author> GenerateAuthors()
        {
            return new List<Author>()
            {
                new Author()
                {
                    FirstName = "John",
                    MiddleNames = new List<string>
                    {
                        "C."
                    },
                    LastName = "Doe",
                    Suffix = "Phd",
                    Email = "John.C.Doe@gmail.com",
                    IsLead = true,
                    IsCorresponding = false,
                    Insitutions = new List<Institution>
                    {
                        new Institution
                        {
                            Name="University of Kansas Medical Center",
                            SubName="Department of Family Medicine"
                        },
                        new Institution {
                            Name="University of Missouri - Kansas City",
                            SubName="School of Medicine"
                        }

                    }
                },
                new Author()
                {
                    FirstName = "Larry",
                    MiddleNames = new List<string>
                    {
                        "Qunicy"
                    },
                    LastName = "Franklin",
                    Suffix = "MD",
                    Email = "Larry.Q.Franklin@gmail.com",
                    CorrespondenceInfo = "Dr. Franklin, Larry.Q.Franklin@gmail.com, Kansas State University, Department of Family Medicine, 100 Main, Manhattan KS, 33456",
                    IsLead = false,
                    IsCorresponding = true,
                    Insitutions = new List<Institution>
                    {
                        new Institution
                        {
                            Name="Kansas State University",
                            SubName="Department of Family Medicine"
                        },
                        new Institution {
                            Name="University of Missouri - Kansas City",
                            SubName="School of Medicine - Department of Family Medicine"
                        }

                    }
                },
                new Author()
                {
                    FirstName = "Douglas",
                    MiddleNames = new List<string>
                    {
                        "Qunicy",
                        "Timothy"
                    },
                    LastName = "Johnson",
                    Suffix = "MD",
                    Email = "DQTJohnson@gmail.com",
                    IsLead = false,
                    IsCorresponding = false,
                    Insitutions = new List<Institution>
                    {
                        new Institution
                        {
                            Name="Kansas State University",
                            SubName="Department of Family Medicine"
                        },
                        new Institution {
                            Name="University of Missouri - Kansas City",
                            SubName="School of Medicine - Department of Family Medicine"
                        }

                    }
                }
            };
        }

        public static List<String> GenerateMetricProviders()
        {
            return new List<String>
            {
                "STFM",
                "Mendeley"
            };
        }
    }

}
