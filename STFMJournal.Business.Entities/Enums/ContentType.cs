﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.Enums
{
    public enum ContentType
    {
        Text,
        Image
    }
}
