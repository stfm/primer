﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MinorEntities
{
    public class Author
    {
        [BsonIgnoreIfNull]
        public string Prefix { get; set; }

        public string FirstName { get; set; }

        [BsonIgnoreIfNull]
        public List<string> MiddleNames { get; set; }
        public string LastName { get; set; }

        [BsonIgnoreIfNull]
        public string Suffix { get; set; }

        [BsonIgnoreIfNull]
        public string Email { get; set; }

        [BsonIgnoreIfNull]
        public string CorrespondenceInfo { get; set; }
        public bool IsLead { get; set; }
        public bool IsCorresponding { get; set; }

        //classes that compose an Author
        [BsonIgnoreIfNull]
        public List<Institution> Insitutions { get; set; }

        //Constructors
        public Author()
        {
            //Assume that the author is not the lead or corresponding author
            //These values will be updated to true on an as needed basis
            IsLead = false;
            IsCorresponding = false;
        }
    }
}
