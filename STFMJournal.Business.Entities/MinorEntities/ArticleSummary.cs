﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MinorEntities
{
    public class ArticleSummary
    {
        public string Title { get; set; }

        [BsonIgnoreIfNull]
        public string Subtitle { get; set; }
        public List<string> Authors { get; set; }
        public DateTimeOffset DatePublished { get; set; }
        public string Category { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string ArticleId { get; set; }

        //Constructors
        private ArticleSummary()
        {
            Authors = new List<string>();
        }

        private ArticleSummary(string title, string subtitle, List<string> authors, DateTimeOffset datePublished, string category, string articleId)
        {
            Title = title;
            Subtitle = subtitle;
            Authors = authors == null ? new List<string>() : authors;
            DatePublished = datePublished;
            Category = string.IsNullOrWhiteSpace(category) ? "none" : category;
            ArticleId = articleId;
        }

        //Factory Create Method - these objects will be constructed from data from the Database 
        public static ArticleSummary Create(string title, string subtitle, List<string> authors, DateTimeOffset datePublished, string category, string articleId)
        {
            var newArticleSummary = new ArticleSummary(title, subtitle, authors, datePublished, category, articleId);
            return newArticleSummary;
        }
    }
}
