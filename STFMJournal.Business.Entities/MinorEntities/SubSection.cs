﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MinorEntities
{
    public class SubSection
    {
        public string Title { get; set; }

        [BsonIgnoreIfNull]
        public string SubTitle { get; set; }

        //Classes that compose a subsection
        public List<ContentArea> ContentAreas { get; set; }
    }
}
