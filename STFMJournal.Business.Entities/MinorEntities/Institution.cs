﻿using MongoDB.Bson.Serialization.Attributes;

namespace STFMJournal.Business.Entities.MinorEntities
{
    public class Institution
    {
        public string Name { get; set; }

        [BsonIgnoreIfNull]
        public string SubName { get; set; }
    }
}