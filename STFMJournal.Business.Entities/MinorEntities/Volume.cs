﻿using STFMJournal.Business.Entities.MajorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace STFMJournal.Business.Entities.MinorEntities
{
    public abstract class Volume
    {
        public string VolumeName { get; set; }
        public string ImageURL { get; set; }

        //Constructors
        protected Volume()
        {

        }

        protected Volume(string volumeName, string imageURL = null)
        {
            VolumeName = volumeName;
            ImageURL = imageURL;
        }
    }
}
