﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MinorEntities
{
    public class Metric
    {
        public string Name { get; set; }

        //Should this be a string, double, decimal? Will a metric ever need to be anything other than an int?
        public int Value { get; set; }


    }
}
