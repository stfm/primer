﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MinorEntities
{
    public class Issue
    {
        public string IssueName { get; set; }
        public string ImageURL { get; set; }
        public List<string> Categories { get; set; }
        public List<ArticleSummary> ArticleSummaries { get; set; }


        //Constructors
        private Issue()
        {
            Categories = new List<string>();
            ArticleSummaries = new List<ArticleSummary>();
        }

        private Issue(string issueName, string imageURL)
        {
            IssueName = issueName;
            ImageURL = imageURL;

            Categories = new List<string>();
            ArticleSummaries = new List<ArticleSummary>();
        }

        //Factory Create Method
        public static Issue Create(string issueName, string imageUrl)
        {
            if (string.IsNullOrWhiteSpace(issueName))
            {
                throw new ArgumentException("The issue name cannot be null or empty.");
            }

            Issue newIssue = new Issue();
            newIssue.IssueName = issueName;
            newIssue.ImageURL = imageUrl;

            return new Issue();
        }

        //Methods
        public void AddArticleSummary(ArticleSummary acticleSummary)
        {
            ArticleSummaries.Add(acticleSummary);
            var category = string.IsNullOrWhiteSpace(acticleSummary.Category) ? "none" : acticleSummary.Category;
            if(Categories.Contains(category) == false)
            {
                Categories.Add(category);
            }
        }
    }
}
