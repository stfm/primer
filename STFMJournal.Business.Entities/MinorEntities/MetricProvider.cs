﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MinorEntities
{
    public interface MetricProvider
    {
        string Name { get; set; }

        //Classes that compose a MetricProvider
        List<Metric> GenerateMetrics();
    }
}
