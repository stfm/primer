﻿using STFMJournal.Business.Entities.MajorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MinorEntities
{
    public class Category
    {
        public string CategoryName { get; set; }

        //The classes that compose a category
        public List<ArticleSummary> Articles { get; set; }
    }
}
