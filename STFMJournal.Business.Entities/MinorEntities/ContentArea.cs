﻿using STFMJournal.Business.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MinorEntities
{
    public class ContentArea
    {
        public string Content { get; set; }
        public ContentType ContentType { get; set; }
    }
}
