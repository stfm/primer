﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MinorEntities
{
    public class Section
    {
        public string Title { get; set; }

        [BsonIgnoreIfNull]
        public string SubTitle { get; set; }

        //Classes that compose a section
        public List<SubSection> SubSections { get; set; }
    }
}
