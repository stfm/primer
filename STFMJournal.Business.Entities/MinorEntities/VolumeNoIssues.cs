﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MinorEntities
{
    public class VolumeNoIssues : Volume
    {
        public List<string> Categories { get; set; }
        public List<ArticleSummary> ArticleSummaries { get; set; }

        //Constructors
        private VolumeNoIssues()
        {
            Categories = new List<string>();
            ArticleSummaries = new List<ArticleSummary>();
        }
        private VolumeNoIssues(string volumeName, string imageURL)
            : base(volumeName, imageURL)
        {
            Categories = new List<string>();
            ArticleSummaries = new List<ArticleSummary>();
        }

        //Factory Create
        public static VolumeNoIssues Create(string volumeName, string imageURL)
        {
            if (string.IsNullOrWhiteSpace(volumeName))
            {
                throw new ArgumentException("In Create method of VolumeNoIssues class. Parameter volumeName cannot be null or empty.");
            }
            var newVolumeNoIssues = new VolumeNoIssues(volumeName, imageURL);
            return newVolumeNoIssues;
        }


        //Methods
        public void AddArticleSummary(ArticleSummary articleSummary)
        {

            ArticleSummaries.Add(articleSummary);
            var category = string.IsNullOrWhiteSpace(articleSummary.Category) ? "none" : articleSummary.Category;
            if(Categories.Contains(category) == false)
            {
                Categories.Add(category);
            }
        }
    }
}
