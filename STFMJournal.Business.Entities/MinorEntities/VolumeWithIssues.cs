﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MinorEntities
{
    public class VolumeWithIssues : Volume
    {
        public List<Issue> Issues { get; set; }

        //Constructors
        private VolumeWithIssues()
        {
            Issues = new List<Issue>();
        }

        private VolumeWithIssues(string volumeName, string imageURL)
        : base(volumeName, imageURL)
        {
            Issues = new List<Issue>();
        }

        //Factory Create
        public static VolumeWithIssues Create(string volumeName, string imageURL)
        {
            if (string.IsNullOrWhiteSpace(volumeName))
            {
                throw new ArgumentException("In Create method of VolumeWithIssues class. Parameter volumeName cannot be null or empty.");
            }

            var newVolumeNoIssues = new VolumeWithIssues(volumeName, imageURL);
            return newVolumeNoIssues;

        }

        //Methods
        public void AddIssue(string issueName, string imageURL)
        {
            if (string.IsNullOrWhiteSpace(issueName))
            {
                throw new ArgumentException("In AddIssue method of VolumeWithIssues class. Parameter issueName cannot be null or empty.");
            }
            if (Issues.Exists(i => i.IssueName == issueName))
            {
                throw new ArgumentException($"The issue {issueName} cannot be added because it already exists.");
            }
            Issues.Add(Issue.Create(issueName, imageURL));
        }

        public void AddArticleSummary(string issueName, ArticleSummary articleSummary)
        {
            if (string.IsNullOrWhiteSpace(issueName))
            {
                throw new ArgumentException("In AddArticle method of VolumeWithIssues class. Parameter issueName cannot be null or empty.");
            }

            if(Issues.Exists(i=> i.IssueName == issueName) == false)
            {
                throw new ArgumentException("In AddArticle method of VolumeWithIssues class. Can't find the issue you are trying to add an ArticleSummary to.");
            }

            Issues.Where(i => i.IssueName == issueName).Single().ArticleSummaries.Add(articleSummary);
        }
    }
}
