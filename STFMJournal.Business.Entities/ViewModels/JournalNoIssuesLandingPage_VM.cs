﻿using STFMJournal.Business.Entities.MinorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.ViewModels
{
    //Journals like PRiMER have no concept of an 'issue'
    public class JournalNoIssuesLandingPage_VM
    {
        
        public string JournalImageURL { get; set; }
        public Volume CurrentVolume { get; set; }
    }
}
