﻿using STFMJournal.Business.Entities.MinorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MajorEntities
{
    public class JournalNoIssues : Journal
    {
        public List<VolumeNoIssues> Volumes { get; set; }

        //Constructors
        private JournalNoIssues()
        {
            Volumes = new List<VolumeNoIssues>();
            HasIssues = false;
        }

        private JournalNoIssues(int createdById)
            : base(createdById)
        {
            Volumes = new List<VolumeNoIssues>();
            HasIssues = false;
        }

        //Factory Create
        public static JournalNoIssues Create(int createdById, string title, string subtitle, string organization, string discipline, string description, string imageURL)
        {
            //Guard Clauses
            if (createdById < 0)
            {
                throw new ArgumentException("In constructor of class VolumeNoIssues, parameter createdById was not a valid id.");
            }

            if (string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentException("In constructor of class VolumeNoIssues, parameter title cannot be null or empty.");
            }

            if (string.IsNullOrWhiteSpace(organization))
            {
                throw new ArgumentException("In constructor of class VolumeNoIssues, parameter organization cannot be null or empty.");
            }

            if (string.IsNullOrWhiteSpace(discipline))
            {
                throw new ArgumentException("In constructor of class VolumeNoIssues, parameter discipline cannot be null or empty.");
            }

            if (string.IsNullOrWhiteSpace(description))
            {
                throw new ArgumentException("In constructor of class VolumeNoIssues, parameter description cannot be null or empty.");
            }

            var newJournalNoIssues = new JournalNoIssues(createdById);
            newJournalNoIssues.Title = title;
            newJournalNoIssues.SubTitle = subtitle;
            newJournalNoIssues.Organization = organization;
            newJournalNoIssues.Discipline = discipline;
            newJournalNoIssues.Description = description;
            newJournalNoIssues.ImageURL = imageURL;

            return newJournalNoIssues;
        }

        //Methods
        public override void AddArticleSummary(string volumeName, string title, string subtitle, List<string> authors,
                            DateTimeOffset datePublished, string category, string articleId, string issueName = null)
        {
            //Guard Clasuses - this is the entry point for adding an article summary
            if (string.IsNullOrWhiteSpace(volumeName))
            {
                throw new ArgumentException("In AddArticleSummary of JournalNoIssues class, parameter volumeName cannot be null or empty.");
            }

            if (string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentException("In AddArticleSummary of JournalNoIssues class, parameter title cannot be null or empty.");
            }

            if (authors == null || authors.Count < 1)
            {
                throw new ArgumentException("In AddArticleSummary of JournalNoIssues class, parameter authors cannot be null or empty.");
            }

            if (string.IsNullOrWhiteSpace(articleId))
            {
                throw new ArgumentException("In AddArticleSummary of JournalNoIssues class, parameter articleId cannot be null or empty.");
            }

            if (Volumes.Exists(v => v.VolumeName == volumeName) == false)
            {
                throw new ArgumentException("In AddArticleSummary of JournalNoIssues class, the volume you are attempting to add an article to could not be found.");
            }

            Volumes.Where(v => v.VolumeName == volumeName)
                .Single()
                .AddArticleSummary(ArticleSummary.Create
                (
                    title, subtitle, authors, datePublished, category, articleId
                ));
        }

        public override void AddVolume(string volumeName, string imageUrl = null)
        {
            if (string.IsNullOrWhiteSpace(volumeName))
            {
                throw new ArgumentException("In AddVolume method of class JournalNoIssues, parameter volumeName cannot be null or empty.");
            }
            if (Volumes.Exists(v => v.VolumeName == volumeName))
            {
                throw new ArgumentException($"In AddVolume method of class JournalNoIssues, the volume {volumeName} already exists.");
            }

            Volumes.Add(VolumeNoIssues.Create(volumeName, imageUrl));
        }

        public int GetVolumeNumberByVolumeName(string volumeName)
        {
            int index = Volumes.FindIndex(v => v.VolumeName == volumeName);
            if (index == -1)
            {
                throw new ArgumentException("In class JournalNoIssues method GetVolumeNumberByVolumeName, the volume name was not found.");
            }

            //Adding one to the index, because people will expect that volumes start counting at 1, not zero
            return index + 1;
        }
    }
}
