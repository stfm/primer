﻿using STFMJournal.Business.Entities.MinorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MajorEntities
{
    public class JournalWithIssues : Journal
    {
        public List<VolumeWithIssues> Volumes { get; set; }

        //Constructors
        private JournalWithIssues()
        {
            Volumes = new List<VolumeWithIssues>();
            HasIssues = true;
        }

        private JournalWithIssues(int createdById) : base(createdById)
        {
            Volumes = new List<VolumeWithIssues>();
            HasIssues = true;
        }

        //Factory Create Method
        public static JournalWithIssues Create(int createdById, string title, string subtitle, string organization, string discipline, string descrtiption, string imageURL)
        {
            //Guard Clauses
            if (createdById < 0)
            {
                throw new ArgumentException("In constructor of class VolumeNoIssues, parameter createdById was not a valid id.");
            }

            if (string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentException("In constructor of class VolumeNoIssues, parameter title cannot be null or empty.");
            }

            if (string.IsNullOrWhiteSpace(organization))
            {
                throw new ArgumentException("In constructor of class VolumeNoIssues, parameter organization cannot be null or empty.");
            }

            if (string.IsNullOrWhiteSpace(discipline))
            {
                throw new ArgumentException("In constructor of class VolumeNoIssues, parameter discipline cannot be null or empty.");
            }

            if (string.IsNullOrWhiteSpace(descrtiption))
            {
                throw new ArgumentException("In constructor of class VolumeNoIssues, parameter descrtiption cannot be null or empty.");
            }

            var newJournalWithIssues = new JournalWithIssues(createdById);
            newJournalWithIssues.Title = title;
            newJournalWithIssues.SubTitle = subtitle;
            newJournalWithIssues.Organization = organization;
            newJournalWithIssues.Discipline = discipline;
            newJournalWithIssues.Description = descrtiption;
            newJournalWithIssues.ImageURL = imageURL;

            return newJournalWithIssues;
        }

        //Methods
        public override void AddArticleSummary(string volumeName, string title, string subtitle, List<string> authors, 
                            DateTimeOffset datePublished, string category, string articleId, string issueName = null)
        {
            //Guard Clasuses - this is the entry point for adding an article summary
            if (string.IsNullOrWhiteSpace(volumeName))
            {
                throw new ArgumentException("In AddArticleSummary of JournalWithIssues class, parameter volumeName cannot be null or empty.");
            }

            if (string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentException("In AddArticleSummary of JournalWithIssues class, parameter title cannot be null or empty.");
            }

            if (authors == null || authors.Count < 1)
            {
                throw new ArgumentException("In AddArticleSummary of JournalWithIssues class, parameter authors cannot be null or empty.");
            }

            if (string.IsNullOrWhiteSpace(articleId))
            {
                throw new ArgumentException("In AddArticleSummary of JournalWithIssues class, parameter articleId cannot be null or empty.");
            }

            if (string.IsNullOrWhiteSpace(issueName))
            {
                throw new ArgumentException("In AddArticleSummary of JournalWithIssues class, parameter issueName cannot be null or empty.");
            }

            if(Volumes.Exists(v=> v.VolumeName == volumeName) == false)
            {
                throw new ArgumentException("In AddArticleSummary of JournalWithIssues class, the volume you are attempting to add an article to could not be found.");
            }

            Volumes.Where(v => v.VolumeName == volumeName).Single().AddArticleSummary(issueName, ArticleSummary.Create
            (
                title, subtitle, authors, datePublished, category, articleId
            ));
        }

        public override void AddVolume(string volumeName, string imageUrl = null)
        {
            if (string.IsNullOrWhiteSpace(volumeName))
            {
                throw new ArgumentException("In AddVolume method of class JournalWithIssues, parameter volumeName cannot be null or empty.");
            }

            if(Volumes.Exists(v=> v.VolumeName == volumeName))
            {
                throw new ArgumentException($"In AddVolume method of class JournalWithIssues, the volume {volumeName} already exists.");
            }

            Volumes.Add(VolumeWithIssues.Create(volumeName, imageUrl));
        }

        public void AddIssue(string volumeName, string issueName, string imageURL = null)
        {
            if (string.IsNullOrWhiteSpace(volumeName))
            {
                throw new ArgumentException($"In AddIssue of class JournalWithIssues, the parameter volumeName cannot be null or empty.");
            }

            if(Volumes.Exists(v => v.VolumeName == volumeName) == false)
            {
                throw new ArgumentException($"In AddIssue of class JournalWithIssues, the volume {volumeName} could not be found.");
            }

            Volumes.Where(v => v.VolumeName == volumeName).Single().AddIssue(issueName, imageURL);
        }

        public int GetVolumeNumberByVolumeName(string volumeName)
        {
            int index = Volumes.FindIndex(v => v.VolumeName == volumeName);
            if (index == -1)
            {
                throw new ArgumentException("In class JournalWithIssues method GetVolumeNumberByVolumeName, the volume name was not found.");
            }

            //Adding one to the index, because people will expect that volumes start counting at 1, not zero
            return index + 1;
        }
    }
}

