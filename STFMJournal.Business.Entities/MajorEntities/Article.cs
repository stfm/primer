﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using STFMJournal.Business.Entities.MinorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MajorEntities
{
    public class Article : MajorEntityBase
    {
        public string Title { get; set; }

        [BsonIgnoreIfNull]
        public string SubTitle { get; set; }
        public string JournalName { get; set; }
        public string Organization { get; set; }
        public DateTimeOffset DatePublished { get; set; }
        public string DOI { get; set; }
        public string ImageURL { get; set; }

        [BsonIgnoreIfNull]
        public int StartingPage { get; set; }

        [BsonIgnoreIfNull]
        public int EndingPage { get; set; }

        public int NumberOfViews { get; set; }
        public int NumberOfDownloads { get; set; }
        public int NumberOfShares { get; set; }



        //Classes that compose an article
        private List<Section> _sections;

        public List<Section> Sections
        {
            get { return _sections; }
            set
            {
                _sections = value;
                DateLastModified = DateTimeOffset.Now;
            }
        }

        private List<Author> _authors;

        public List<Author> Authors
        {
            get { return _authors; }
            set
            {
                _authors = value;
                DateLastModified = DateTimeOffset.Now;
            }
        }

        private List<string> _metricProviders;

        
        public List<String> MetricProviders
        {
            get { return _metricProviders; }
            set
            {
                _metricProviders = value;
                DateLastModified = DateTimeOffset.Now;
            }
        }

        /*******************Constructors*********************/

        //Mongo C# Driver requires a parameterless constructor
        private Article()
        {
            //Initialize all metric counts to zero
            NumberOfDownloads = 0;
            NumberOfShares = 0;
            NumberOfViews = 0;

            //Initializing lists to avoid null reference exceptions
            Authors = new List<Author>();
            Sections = new List<Section>();
            MetricProviders = new List<String>();
        }

        private Article(int createdById)
        : base(createdById)
        {
            //Initialize all metric counts to zero
            NumberOfDownloads = 0;
            NumberOfShares = 0;
            NumberOfViews = 0;

            //Initializing lists to avoid null reference exceptions
            Authors = new List<Author>();
            Sections = new List<Section>();
            MetricProviders = new List<String>();
        }

        //Article factory method

        //An article MUST have the following in order to be created:
        // -- createdById (Id of the user who's creating the article)
        // -- title
        // -- a list of Sections where each section has at least one subsection (this means the article must have some content)
        // -- a list of Authors with at least one Author (this means at least one author exists for the article)

        public static Article Create(int createdById, string title, string subTitle, string journalName, DateTimeOffset pubDate,List<Section> sections, List<Author> authors, List<String> metricProviders)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("The article must have a title.");
            }
            if (string.IsNullOrEmpty(journalName))
            {
                throw new ArgumentException("A journal name must be provided for the article.");
            }
            if (sections == null || sections.Count == 0)
            {
                throw new ArgumentException("The article must contain at least one section.");
            }

            if (authors == null || authors.Count == 0)
            {
                throw new ArgumentException("The article must contain at least one author.");
            }

            if (createdById < 0)
            {
                throw new ArgumentException("An article must contain a valid user Id");
            }

            Article newArticle = new Article(createdById);
            newArticle.Title = title;
            newArticle.SubTitle = string.IsNullOrEmpty(subTitle) ? "" : subTitle;
            newArticle.JournalName = journalName;
            newArticle.DatePublished = pubDate;
            newArticle.Sections = sections;
            newArticle.Authors = authors;
            newArticle.MetricProviders = metricProviders == null || metricProviders.Count == 0 ? new List<String>() : metricProviders;

            return newArticle;
        }

    }
}
