﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MajorEntities
{
    public abstract class MajorEntityBase
    {
        //Mongo ObjectIds come built in with a timestamp.This can be used to find the "created on" date
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; private set; }

        //Properties that represent housekeeping information for major entities
        public DateTimeOffset DateLastModified { get; protected set; }
        public int CreatedById { get; private set; }
        public int LastModifiedById { get; private set; }

        //Properties not mapped to the database
        public DateTimeOffset DateCreated => Id == null ? DateTimeOffset.Now : ObjectId.Parse(Id).CreationTime;


        //Public Methods
        public void UpdateLastModificationInfo(int lastModifiedById)
        {
            DateLastModified = DateTimeOffset.Now;
            LastModifiedById = lastModifiedById;
        }


        /*******************Constructors*********************/

        //Mongo C# Driver requires a parameterless constructor
        protected MajorEntityBase()
        {

        }


        //Creating a new article
        protected MajorEntityBase(int createdById)
        {
            CreatedById = LastModifiedById =  createdById;
            DateLastModified = DateTimeOffset.Now;
        }

    }
}
