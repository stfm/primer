﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using STFMJournal.Business.Entities.MinorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.MajorEntities
{
    public abstract class Journal : MajorEntityBase
    {
        public string Title { get; set; }

        [BsonIgnoreIfNull]
        public string SubTitle { get; set; }
        public string Organization { get; set; }
        public string Discipline { get; set; }
        public string Description { get; set; }
        [BsonIgnoreIfNull]
        public string ImageURL { get; set; }
        public bool HasIssues { get; set; }

        //Constructors

        //Mongo requrires a parameterless constructor
        protected Journal()
        {

        }

        protected Journal(int createdById)
        : base(createdById)
        {

        }


        //Methods
        public abstract void AddVolume(string volumeName, string imageUrl = null);

        public abstract void AddArticleSummary(string volumeName, string title, string subtitle, 
                                               List<string> authors, DateTimeOffset datePublished, 
                                               string category, string articleId, string issueName = null);

    }
}
