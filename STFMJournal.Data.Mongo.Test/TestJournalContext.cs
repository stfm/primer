﻿using MongoDB.Driver;
using STFMJournal.Business.Entities.MajorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Data.Mongo.Test
{
    public class TestJournalContext
    {
        public IMongoDatabase Database;

        public TestJournalContext()
        {
            var client = new MongoClient("mongodb://localhost");
            Database = client.GetDatabase("JournalTestDatabase");
        }

        public IMongoCollection<Article> Articles
        {
            get
            {
                return Database.GetCollection<Article>("articles");
            }
        }
    }
}
