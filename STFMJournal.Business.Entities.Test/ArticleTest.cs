﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using STFMJournal.Business.Entities.MajorEntities;
using STFMJournal.Business.Entities.MinorEntities;
using System;
using System.Collections.Generic;
using STFMJournal.Common.Mock;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.Test
{
    [TestClass]
    public class ArticleTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Article_NoTitle_ThrowsException()
        {
            //Arrange & Act
            try
            {
                Article newArticle = Article.Create(1, null, "Have we gotten it wrong for the last 50 years?", "PRiMER", DateTimeOffset.Now, ArticleMock.GenerateSections(),
                    ArticleMock.GenerateAuthors(), ArticleMock.GenerateMetricProviders());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Article_NegativeCreatedById_ThrowsException()
        {

            //Arrange & Act
            try
            {
                Article newArticle = Article.Create(-1, "Nutrition Revolution", "Have we gotten it wrong for the last 50 years?", "PRiMER", DateTimeOffset.Now, ArticleMock.GenerateSections(),
                    ArticleMock.GenerateAuthors(), ArticleMock.GenerateMetricProviders());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Article_NullSections_ThrowsException()
        {
            //Arrange & Act
            try
            {
                Article newArticle = Article.Create(1, "Nutrition Revolution", "Have we gotten it wrong for the last 50 years?", "PRiMER", DateTimeOffset.Now, null,
                    ArticleMock.GenerateAuthors(), ArticleMock.GenerateMetricProviders());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Article_EmptySections_ThrowsException()
        {
            //Arrange & Act
            try
            {
                Article newArticle = Article.Create(1, "Nutrition Revolution", "Have we gotten it wrong for the last 50 years?", "PRiMER", DateTimeOffset.Now, new List<Section>(),
                    ArticleMock.GenerateAuthors(), ArticleMock.GenerateMetricProviders());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Article_NullAuthors_ThrowsException()
        {
            //Arrange & Act
            try
            {
                Article newArticle = Article.Create(1, "Nutrition Revolution", "Have we gotten it wrong for the last 50 years?", "PRiMER", DateTimeOffset.Now, ArticleMock.GenerateSections(),
                    null, ArticleMock.GenerateMetricProviders());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Article_EmptyAuthors_ThrowsException()
        {
            //Arrange & Act
            try
            {
                Article newArticle = Article.Create(1, "Nutrition Revolution", "Have we gotten it wrong for the last 50 years?", "PRiMER", DateTimeOffset.Now, ArticleMock.GenerateSections(),
                    new List<Author>(), ArticleMock.GenerateMetricProviders());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        //Note: in order to test the DateCreated property, we need MongoDb. The date created is pulled from the date timestamp on the ObjectId,
        //This is all provided by Mongo when the Article is inserted into the database for the first time.
        [TestMethod]
        public void Article_EntityBaseProperlyInitialized_DateLastModified()
        {
            //Arrange
            var expected = DateTimeOffset.Now.Date.ToShortDateString();

            //Act
            Article newArticle = Article.Create(1, "Nutrition Revolution", "Have we gotten it wrong for the last 50 years?", "PRiMER", DateTimeOffset.Now, ArticleMock.GenerateSections(),
                ArticleMock.GenerateAuthors(), ArticleMock.GenerateMetricProviders());
            var actual = newArticle.DateLastModified.Date.ToShortDateString();

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Article_EntityBaseProperlyInitialized_CreatedById()
        {
            //Arrange
            var createdById = 1;
            var expected = createdById;

            //Act
            Article newArticle = Article.Create(createdById, "Nutrition Revolution", "Have we gotten it wrong for the last 50 years?", "PRiMER", DateTimeOffset.Now, ArticleMock.GenerateSections(),
                ArticleMock.GenerateAuthors(), ArticleMock.GenerateMetricProviders());
            var actual = newArticle.CreatedById;

            //Assert
            Assert.AreEqual(expected, actual);

        }
    }
}
