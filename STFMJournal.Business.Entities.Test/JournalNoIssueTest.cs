﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using STFMJournal.Business.Entities.MajorEntities;
using STFMJournal.Business.Entities.MinorEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Business.Entities.Test
{
    [TestClass]
    public class JournalNoIssueTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssue_NoTitle_ThrowsException()
        {
            //Act & Arrange
            try
            {
                JournalNoIssues newJournal = JournalNoIssues.Create(1, null, "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssue_EmptyTitle_ThrowsException()
        {
            //Act & Arrange
            try
            {
                JournalNoIssues newJournal = JournalNoIssues.Create(1, "", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssue_NegativeUserId_ThrowsException()
        {
            //Act & Arrange
            try
            {
                JournalNoIssues newJournal = JournalNoIssues.Create(-1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssue_NullOrganization_ThrowsException()
        {
            //Act & Arrange
            try
            {
                JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", null, "Family Medicine", "Great journal about family medicine", null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssue_EmptyOrganization_ThrowsException()
        {
            //Act & Arrange
            try
            {
                JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "", "Family Medicine", "Great journal about family medicine", null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssue_EmptyDiscipline_ThrowsException()
        {
            //Act & Arrange
            try
            {
                JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "", "Great journal about family medicine", null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssue_NullDiscipline_ThrowsException()
        {
            //Act & Arrange
            try
            {
                JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", null, "Great journal about family medicine", null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssue_NullDescription_ThrowsException()
        {
            //Act & Arrange
            try
            {
                JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", null, null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssue_EmptyDescription_ThrowsException()
        {
            //Act & Arrange
            try
            {
                JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "", null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        [TestMethod]
        public void JournalNoIssue_Volumes_InitializedToEmptyList()
        {
            //Arrange
            var expected = new List<Volume>();
            JournalNoIssues newJournal;

            //Act
            newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            var actual = newJournal.Volumes;

            //Assert
            Assert.AreEqual(expected.Count, actual.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssues_AddVolumeThatAlreadyExists_ThrowsException()
        {
            //Arrange
            JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);

            //Act
            newJournal.AddVolume("2016");
            newJournal.AddVolume("2016");
        }

        [TestMethod]
        public void JournalNoIssues_GetVolumeNumberByVolumeName()
        {
            //Arrange
            JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            var expected = 1;

            //Act
            newJournal.AddVolume("2016");
            var actual = newJournal.GetVolumeNumberByVolumeName("2016");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void JournalNoIssues_AddMultipleArticlesWithSameCategory_CategoriesListDoesNotContainDuplicates()
        {
            //Arrange
            JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            newJournal.AddVolume("2015");

            //Act
            newJournal.AddArticleSummary("2015", "Nice Title", null, Mocker.GenerateAuthors(), DateTimeOffset.Now, "Letter to the Editor", "507f1f77bcf86cd799439012");
            newJournal.AddArticleSummary("2015", "Another Nice Title", null, Mocker.GenerateAuthors(), DateTimeOffset.Now, "Letter to the Editor", "507f1f77bcf86cd799439013");
            newJournal.AddArticleSummary("2015", "One more Nice Title", null, Mocker.GenerateAuthors(), DateTimeOffset.Now, "Letter to the Editor", "507f1f77bcf86cd799439014");

            //Assert
            Assert.AreEqual(1, newJournal.Volumes.Where(v => v.VolumeName == "2015").Single().Categories.Count);
            Assert.AreEqual(true, newJournal.Volumes.Where(v => v.VolumeName == "2015").Single().Categories.Contains("Letter to the Editor"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssues_AddArticleSummaryWithNullTitle_ThrowsArgumentException()
        {
            //Arrange
            JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            newJournal.AddVolume("2015");

            //Act
            newJournal.AddArticleSummary("2015", null, "A nice subtitle", Mocker.GenerateAuthors(), DateTimeOffset.Now, "Op Ed", Mocker.GenerateObjectId());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssues_AddArticleSummaryWithEmptyitle_ThrowsArgumentException()
        {
            //Arrange
            JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            newJournal.AddVolume("2015");

            //Act
            newJournal.AddArticleSummary("2015", "", "A nice subtitle", Mocker.GenerateAuthors(), DateTimeOffset.Now, "Op Ed", Mocker.GenerateObjectId());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssues_AddArticleSummarNullAuthors_ThrowsArgumentException()
        {
            //Arrange
            JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            newJournal.AddVolume("2015");

            //Act
            newJournal.AddArticleSummary("2015", "A nice title", "A nice subtitle", null, DateTimeOffset.Now, "Op Ed", Mocker.GenerateObjectId());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssues_AddArticleSummarEmptyAuthors_ThrowsArgumentException()
        {
            //Arrange
            JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            newJournal.AddVolume("2015");

            //Act
            newJournal.AddArticleSummary("2015", "A nice title", "A nice subtitle", new List<string>(), DateTimeOffset.Now, "Op Ed", Mocker.GenerateObjectId());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssues_AddArticleSummaryWithNullArticleId_ThrowsArgumentException()
        {
            //Arrange
            JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            newJournal.AddVolume("2015");

            //Act
            newJournal.AddArticleSummary("2015", "A nice Title", "A nice subtitle", Mocker.GenerateAuthors(), DateTimeOffset.Now, "Op Ed", "Great journal about family medicine", null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssues_AddArticleSummaryWithEmptyArticleId_ThrowsArgumentException()
        {
            //Arrange
            JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            newJournal.AddVolume("2015");

            //Act
            newJournal.AddArticleSummary("2015", "A Nice title", "A nice subtitle", Mocker.GenerateAuthors(), DateTimeOffset.Now, "Op Ed", "");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssues_AddArticleSummaryWithNullVolumeName_ThrowsArgumentException()
        {
            //Arrange
            JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            newJournal.AddVolume("2015");

            //Act
            newJournal.AddArticleSummary(null, "A nice Title", "A nice subtitle", Mocker.GenerateAuthors(), DateTimeOffset.Now, "Op Ed", Mocker.GenerateObjectId());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void JournalNoIssues_AddArticleSummaryWithEmptyVolumeName_ThrowsArgumentException()
        {
            //Arrange
            JournalNoIssues newJournal = JournalNoIssues.Create(1, "A nice title", "A Nice Subtitle...", "STFM", "Family Medicine", "Great journal about family medicine", null);
            newJournal.AddVolume("2015");

            //Act
            newJournal.AddArticleSummary("", "A Nice title", "A nice subtitle", Mocker.GenerateAuthors(), DateTimeOffset.Now, "Op Ed", Mocker.GenerateObjectId());
        }
    }

    public static class Mocker
    {
        public static List<string> GenerateAuthors()
        {
            return new List<string>()
            {
                "Bob Johnson, Phd",
                "Laura M. Dickson, Md",
                "John F. Frankling, Md"
            };
        }

        public static string GenerateObjectId()
        {
            return ObjectId.GenerateNewId().ToString();
        }
    }
}
