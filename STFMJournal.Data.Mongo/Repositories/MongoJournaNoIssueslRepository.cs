﻿using STFMJournal.Data.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using STFMJournal.Business.Entities.MajorEntities;
using STFMJournal.Business.Entities.MinorEntities;

namespace STFMJournal.Data.Mongo.Repositories
{
    public class MongoJournaNoIssueslRepository : IJournalNoIssuesRepository
    {
        private JournalContext context;

        public MongoJournaNoIssueslRepository(IMongoConnectionObject connectionObject)
        {
            context = new JournalContext(connectionObject.ConnectionString, connectionObject.DatabaseName);
        }



        public void Create(JournalNoIssues journal)
        {
            try
            {
                context.JournalsNoIssues.InsertOne(journal);
            }
            catch(BsonSerializationException ex)
            {
                //Log to db
                throw ex;
            }
            catch(Exception ex)
            {
                //log to db
                throw ex;
            }
        }

        public void Delete(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public List<JournalNoIssues> GetAll()
        {
            throw new NotImplementedException();
        }

        public List<ArticleSummary> GetArticlesByIssue(ObjectId journalId, int volume, int issue)
        {
            throw new NotImplementedException();
        }

        public JournalNoIssues GetById(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public List<ArticleSummary> GetRecentArticles(ObjectId journalId, int numberOfArticlesToGet)
        {
            throw new NotImplementedException();
        }

        public List<Issue> GetRecentIssues(ObjectId journalId, int numberOfIssuesToGet)
        {
            throw new NotImplementedException();
        }

        public void Update(JournalNoIssues journal)
        {
            throw new NotImplementedException();
        }

        
    }
}
