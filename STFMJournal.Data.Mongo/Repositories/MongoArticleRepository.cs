﻿using STFMJournal.Data.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using STFMJournal.Business.Entities.MajorEntities;
using MongoDB.Driver;

namespace STFMJournal.Data.Mongo.Repositories
{
    public class MongoArticleRepository : IArticleRepository
    {

        private JournalContext context;

        public MongoArticleRepository(IMongoConnectionObject connectionObject)
        {
            context = new JournalContext(connectionObject.ConnectionString, connectionObject.DatabaseName);
        }

        public void AddArticle(Article article)
        {
            try
            {
                context.Articles.InsertOne(article);
            }
            catch (MongoWriteException ex)
            {
                //Log error here
                throw ex;
            }

        }

        public void DeleteArticle(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public Article GetArticleById(string id)
        {
            try
            {
                return context.Articles
                    .Find(a => a.Id == id)
                    .FirstOrDefault();
            }
            catch (Exception ex)
            {
                //log exception
                throw ex;
            }
        }

        public Article GetArticleById(ObjectId id)
        {
            try
            {
                return context.Articles
                    .Find(a => a.Id == id.ToString())
                    .FirstOrDefault();
            }
            catch (Exception ex)
            {
                //log exception
                throw ex;
            }
        }

        public void UpdateArticle(ObjectId id, Article article)
        {
            throw new NotImplementedException();
        }
    }
}
