﻿using MongoDB.Driver;
using STFMJournal.Business.Entities.MajorEntities;
using STFMJournal.Data.Mongo.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Data.Mongo
{
    public class JournalContext
    {
        public IMongoDatabase Database;

        public JournalContext(string connectionString, string databaseName)
        {
            var client = new MongoClient(connectionString);
            Database = client.GetDatabase(databaseName);
        }

        public IMongoCollection<Article> Articles
        {
            get
            {
                return Database.GetCollection<Article>("articles");
            }
        }

        public IMongoCollection<JournalNoIssues> JournalsNoIssues
        {
            get
            {
                return Database.GetCollection<JournalNoIssues>("journalsnoissues");
            }
        }

        public IMongoCollection<JournalWithIssues> JournalsWithIssues
        {
            get
            {
                return Database.GetCollection<JournalWithIssues>("journalswithissues");
            }
        }
    }
}
