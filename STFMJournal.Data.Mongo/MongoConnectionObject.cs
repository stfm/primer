﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STFMJournal.Data.Mongo
{
    public interface IMongoConnectionObject
    {
        string ConnectionString { get;  set; }
        string DatabaseName { get;  set; }
    }
}
